package chapter3;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Spliterator;
import java.util.function.Consumer;

public class MyLinkedList<Integer> implements Iterable<Integer> {
    private int theSize;
    private int modCount = 0;
    private Node<Integer> beginMarker;
    private Node<Integer> endMarker;

    private static class Node<Integer>{
        public Integer data;
        public Node<Integer> prev;
        public Node<Integer> next;
        public Node(Integer d,Node<Integer>p,Node<Integer>n){
            data = d;
            prev = p;
            next = n;
        }
    }

    public MyLinkedList(){doClear();}
    public void clear(){doClear();}
    public void doClear(){
        beginMarker = new Node<Integer>(null,null,null);
        endMarker = new Node<Integer>(null,beginMarker,null);
        beginMarker.next = endMarker;
        theSize = 0;
        modCount++;
    };
    public int size(){
        return theSize;
    }

    public boolean isEmpty(){
        return size() == 0;
    }

    public boolean add(Integer x){
        add(size(),x);
        return true;
    }

    public void add(int idx,Integer x){
        addBefore(getNode(idx,0,size()),x);
    }
    private void addBefore(Node<Integer> p,Integer x){
        Node<Integer> newNode = new Node<>(x,p.prev,p);
        newNode.prev.next = newNode;
        p.prev = newNode;
        theSize++;
        modCount++;
    }
    public Integer get(int idx){
        return getNode(idx).data;
    }

    public Integer set(int idx,Integer newVal){
        Node<Integer> p = getNode(idx);
        Integer oldVal = p.data;
        p.data = newVal;
        return newVal;
    }

    public Integer remove(int idx){
        return remove(getNode(idx));
    }



    private Integer remove(Node<Integer> p){
        p.next.prev = p.prev;
        p.prev.next = p.next;
        theSize --;
        modCount ++;
        return p.data;
    }

    private Node<Integer> getNode(int idx){
        return getNode(idx,0,size()-1);
    }

    private Node<Integer> getNode(int idx,int lower,int upper){
        Node<Integer> p;
        if(idx<lower||idx>upper){
            throw new IndexOutOfBoundsException();
        }
        if(idx<size()/2){
            p = beginMarker.next;
            for(int i=0;i<idx;i++){
                p=p.next;
            }
        }else {
            p=endMarker;
            for (int i=size();i>idx;i--){
                p = p.prev;
            }
        }
        return p;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new LinkedListItertrator();
    }

    private class LinkedListItertrator implements Iterator<Integer> {
        private Node<Integer> current = beginMarker.next;
        private int expecteModCount = modCount;
        private boolean okToRemove = false;

        @Override
        public boolean hasNext() {
            return current != endMarker;
        }

        @Override
        public Integer next() {
            if(modCount != expecteModCount){
                throw new ConcurrentModificationException();
            }
            if(!hasNext()){
                throw new NoSuchElementException();
            }
            Integer nextItem = current.data;
            current = current.next;
            okToRemove = true;
            return nextItem;
        }

        @Override
        public void remove() {
            if(modCount != expecteModCount){
                throw new ConcurrentModificationException();
            }
            if(!okToRemove){
                throw new IllegalStateException();
            }

            MyLinkedList.this.remove(current.prev);
            expecteModCount++;
            okToRemove = false;
        }
    }

    @Override
    public Spliterator<Integer> spliterator() {
        return null;
    }

    @Override
    public void forEach(Consumer<? super Integer> action) {

    }

    public boolean contains(Integer e) {
        // TODO Auto-generated method stub
        Node<Integer> current = beginMarker;
        if(current.data.equals(e)) {
            return true;
        }
        for(int i = 1; i < theSize; i++) {
            current = current.next;
            if(current.data.equals(e)) {
                return true;
            }
        }
        return false;
    }
}
