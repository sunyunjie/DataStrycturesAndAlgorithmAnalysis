package chapter3;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Chapter3Test {
    /*给定一个表L和另一个表P, 它们包含以升序排列的整数。操作printLots(L, P)将打印L中
那些由P所指定的位置上的元素。例如，如果P=l, 3, 4, 6, 那么，L中位于第1、第3、第4和
第6个位置上的元素被打印出来。写出过程printLots (L, P)。只可使用public型的
Collecti ons API容器操作。该过程的运行时间是多少？*/
    public static void printLots(List l, List p){
        Iterator<Integer> iteratorL = l.iterator();
        Iterator<Integer> iteratorP = l.iterator();
        Integer llots = iteratorL.next();
        Integer plots = iteratorP.next();
        int index = 1;
        while(llots != null && plots !=null){
            if(plots == index ++){
                System.out.print(llots);
            }
            if(iteratorP.hasNext()){
                plots = iteratorP.next();
            }else {break;
            }
        }
        llots = iteratorL.next();
    }

    public static void main(String[] args) {
        List l = new LinkedList();
        List p = new LinkedList();
        l.add(5);l.add(6);l.add(9);l.add(1);l.add(3);l.add(2);
        p.add(1);  p.add(3);   p.add(5);
        printLots(l,p);

    }


}
