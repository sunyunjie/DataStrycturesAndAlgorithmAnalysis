package chapter3;


import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Spliterator;
import java.util.function.Consumer;

public class MyArrayList implements Iterable<Integer> {
    private static final int DEFAULT_CAPACITY = 10;
    private int theSize;
    private Integer[] theItems;
    public MyArrayList(){
        doClear();
    }
    public void clear(){
        doClear();
    }
    private void doClear(){
        theSize = 0;
        ensureCapacity(DEFAULT_CAPACITY);
    }
    public int size(){
        return theSize;
    }
    public boolean isEmpty(){
        return size() == 0 ;
    }
    public void trimToSize(){
        ensureCapacity(size());
    }
    public Integer get(int idx){
        if(idx<0||idx>=size()){
            throw new ArrayIndexOutOfBoundsException();
        }

        return theItems[idx];
    }

    public Integer set(int idx,Integer newVal){
        if(idx<0||idx>=size()){
            throw new ArrayIndexOutOfBoundsException();
        }
        Integer old = theItems[idx];
        theItems[idx] = newVal;

        return old;
    }

    public void ensureCapacity(int newCapacity){
        if(newCapacity < theSize){
            return;
        }

        Integer[] old = theItems;
        theItems = (Integer[]) new Object[newCapacity];
        for(int i=0;i<size();i++){
            theItems[i] = old[i];
        }
    }

    public boolean add(Integer x){
        add(size(),x);
        return true;
    }

    public void add(int idx,Integer x){
        if(theItems.length == size()){
            ensureCapacity(size()*2+1);
        }
        for(int i = theSize;i>idx;i--){
            theItems[i] = theItems[i-1];
        }
        theItems[idx] = x;

        theSize++;
    }

    public Integer remove(int idx){
        Integer removedItem = theItems[idx];
        for(int i=idx;i<size()-1;i++){
            theItems[i] = theItems[i+1];
        }
        theSize--;
        return removedItem;
    }
    @Override
    public Iterator iterator() {
        return null;
    }

    private class ArrayListIterator implements Iterator<Integer>{
        private  int current = 0 ;
        @Override
        public boolean hasNext() {
            return current < size();
        }

        @Override
        public Integer next() {
            if(!hasNext()){
                throw  new NoSuchElementException();
            }
            return theItems[current];
        }

        @Override
        public void remove() {
            MyArrayList.this.remove(--current);
        }
    }


    @Override
    public void forEach(Consumer action) {

    }

    @Override
    public Spliterator spliterator() {
        return null;
    }
}
