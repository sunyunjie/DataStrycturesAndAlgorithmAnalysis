package chapter4;

public class AvlTree {
    private static class AvlNode<Integer>{
        Integer element;
        AvlNode<Integer> left;
        AvlNode<Integer> right;
        int height;
        AvlNode(Integer integer){
            this(integer,null,null);
        }
        AvlNode(Integer integer,AvlNode<Integer> lt,AvlNode<Integer> rt){
            element = integer;
            left = lt;
            right = rt;
            height = 0;
        }
    }
    private int height(AvlNode<Integer> t){
        return t == null?-1:t.height;
    }

    private AvlNode<Integer> insert(Integer x,AvlNode<Integer> t){
        if(t == null){
            return new AvlNode<>(x,null,null);
        }

        int compareResult = x.compareTo(t.element);
        if(compareResult<0){
            t.left = insert(x,t.left);
        }else if (compareResult > 0){
            t.right = insert(x,t.right);
        }
        return balance(t);
    }
    private static final int AllOWED_IMBALANCE = 1;
    private AvlNode<Integer> balance(AvlNode<Integer> t){
        /*if(t==null){
            return t;
        }

        if(height(t.left)-height(t.right)>AllOWED_IMBALANCE){
            if(height(t.left.left)>=height(t.left.right)){
                t = rotateWithLeftChild(t);
            }else {
                t = doubleWithLeftChild(t);
            }

        }else {
            if(height(t.right)-height(t.left)>AllOWED_IMBALANCE){
                if(height(t.right.right)>=height(t.right.left)){
                    t = rotateWithRightChild(t);
                }else {
                    t = doubleWithRightChild(t);
                }
            }
        }
        t. height = Math.max(height(t.left),height(t.right)) + 1;*/
        return t;
    }

        private AvlNode<Integer> rotateWithLeftChild (AvlNode<Integer> k2){
            AvlNode<Integer> k1 = k2. left;
            k2.left = k1.right;
            k1.right = k2;
            k2.height = Math.max(height(k2. left) , height(k2.right)) + 1;
            k1.height = Math.max(height(k1. left) , k2.height) + 1;
            return k1;
        }


}
