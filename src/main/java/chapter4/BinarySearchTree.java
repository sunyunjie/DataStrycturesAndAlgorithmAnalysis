package chapter4;

import com.sun.xml.internal.bind.AnyTypeAdapter;

public class BinarySearchTree<Integer extends Comparable<Integer>> {

        private static class BinaryNode<Integer>{
            Integer element;
            BinaryNode<Integer> left;
            BinaryNode<Integer> right;
            BinaryNode(Integer integer){
                this(integer,null,null);
            }
            BinaryNode(Integer integer,BinaryNode<Integer> lt,BinaryNode<Integer> rt){
                element = integer;
                left = lt;
                right = rt;
            }
        }

        private BinaryNode<Integer> root;

    public BinarySearchTree() {
        root = null;
    }

    private boolean contains(Integer x,BinaryNode<Integer> t){
        if(t==null){return false;}
        int compareResult = x.compareTo(t.element);
        if(compareResult<0){
            return contains(x,t.left);
        }else if(compareResult>0){
            return contains(x,t.right);
        }else {
            return true;
        }
    }

    private BinaryNode<Integer> findMin(BinaryNode<Integer> t){
        if(t == null){
            return null;
        }else if(t.left == null){
            return t;
        }
        return findMin(t.left);
    }

    private BinaryNode<Integer> findMax(BinaryNode<Integer> t){
        if(t == null){
            return null;
        }else if(t.right == null){
            return t;
        }
        return findMax(t.right);
    }

    private BinaryNode<Integer> insert(Integer x,BinaryNode<Integer> t){
        if(t==null){
            return new BinaryNode<>(x,null,null);
        }

        int compareResult = x.compareTo(t.element);

        if(compareResult<0){
             t.left = insert(x,t.left);
        }else if(compareResult>0){
             t.right=insert(x,t.right);
        }else {

        }
             return t;
    }

    private BinaryNode<Integer> remove(Integer x,BinaryNode<Integer> t){
        if(t==null){
            return t;
        }

        int compareResult = x.compareTo(t.element);

        if(compareResult<0){
            t.left = remove(x,t.left);
        }else if(compareResult>0){
            t.right=remove(x,t.right);
        }else if(t.left !=null && t.right !=null) {//two chile
            t.element = findMin(t.right).element;
            t.right = remove(t.element,t.right);

        }else {
            t=(t.left!=null)?t.left:t.right;
        }

        return t;
    }
}
