package chapter6;

public class BinaryHeap<E extends Comparable<? super E>> {
    private static final int DEFAULT_CAPACITY = 10;
    private int currentSize;
    private E[] array;

    public BinaryHeap(){

    }
    public BinaryHeap(int capacity){}
    public BinaryHeap(E[] items){
        currentSize = items.length;
        array = (E[])new Comparable[(currentSize+2)*11/10];

        int i =1;
        for(E item:items){
            array[i++] = item;
        }
        buildHeap();
    }
    public void insert(E x){
        if(currentSize == array.length-1) {
            enlargeArray(array.length * 2 + 1);
        }
            int hole = ++currentSize;
            for(array[0] = x;x.compareTo(array[hole/2])<0;hole/=2){
                array[hole] = array[hole/2];

            }
            array[hole] = x;

    }
    public E findMin(){ E minItem = findMin(); return minItem; };
    public E deleteMin(){
        if(isEmpty()){}

        E minItem = findMin();
        array[1] = array[currentSize--];
        percolateDown(1);
        return minItem;
    };
    public boolean isEmpty(){return true;};
    public void makeEmpty(){};

    private void percolateDown(int hole){
        int child;
        E tmp = array[hole];
        for(;hole*2<=currentSize;hole = child){
            child = hole*2;
            if(child!=currentSize&&array[child+1].compareTo(array[child])<0){
                child++;
                if(array[child].compareTo(tmp)<0){
                    array[hole] = array[child];
                }else {break;}
            }
        }
        array[hole] = tmp;

    }
    private void buildHeap(){
        for(int i=currentSize/2;i>0;i--){
            percolateDown(i);
        }
    }
    private void enlargeArray(int newSize){}
}
