package chapter7;

public class HeapSort <E extends Comparable<? super E>> {
    private static int leftChild(int i){
        return 2*i+1;
    }

    void precDown(E[] a,int i,int n){
        int child;
        E tmp;
        for(tmp = a[i];leftChild(i)<n;i=child){
            child = leftChild(i);
            if(child!=n-1&&a[child].compareTo(a[child+1])<0){
                child++;
            }
            if(tmp.compareTo(a[child])<0){
                a[i] = a[child];
            }else {break;}
        }
        a[i] = tmp;
    }
    void heapsort(E[] a){
        for(int i=a.length/2-1;i>=0;i--){
            precDown(a,i,a.length);
        }
        for (int i=a.length-1;i>0;i--){
           //deleteMac
            precDown(a,0,i);
        }
    }


}
