package chapter7;

public class SortDemo  <E extends Comparable<? super E>>{

    public  void insert(E[] a){
        int j;
        for(int p=1;p<a.length;p++){
           E tmp = a[p];
           for(j=p;j>0&&tmp.compareTo(a[j-1])<0;j--){
               a[j] = a[j-1];
           }
           a[j] = tmp;
        }    System.out.print(a);
    }
    //   Integer[] a = {3,1,5,2,4};
    public int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];;
        for(int i=0;i<nums.length;i++){
            for (int j=i+1;j<nums.length;j++){
                if(nums[i]+nums[j]==target){
                    result[0] = i;
                    result[1] = j;
                }
            }
        }
        return result;
    }
    public void shellsort(E[] a){
        int j;
        for(int gap = a.length/2;gap>0;gap/=2){
            for(int i=gap;i<a.length;i++){
                E tmp = a[i];
                    for(j=i;j>=gap&&tmp.compareTo(a[j-gap])<0;j-=gap){
                        a[j] = a[j-gap];
                    }
                a[j] = tmp;
            }
        }
    }

}
