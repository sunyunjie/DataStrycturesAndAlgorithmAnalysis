package chapter5;

import java.util.LinkedList;
import java.util.List;

public class SeparateChainingHashTable<E extends Comparable<? super E>> {

    private static final int DEFAULT_TABLE_SIZE = 101;
    private List<E>[ ] theLists;
    private int currentSize;
    public SeparateChainingHashTable(){
        this(DEFAULT_TABLE_SIZE);
    }
    public SeparateChainingHashTable(int size){

        theLists = new LinkedList[nextPrime(size)];
        for(int i=0;i<theLists.length;i++){
            theLists[i] = new LinkedList<>();
        }
    }
    public void insert(E x){
        List<E> whichList = theLists[myhash(x)];
        if(!whichList.contains(x)){
            whichList.add(x);
            if(++currentSize>theLists.length){
                rehash();
            }
        }
    }
    public void remove(E x){
        List<E> wichList = theLists[myhash(x)];
        if(wichList.contains(x)){
            wichList.remove(x);
            currentSize --;
        }
    }
    public boolean contains(E x){
        List<E> wichList = theLists[myhash(x)];
        return wichList.contains(x);
    }
    public void makeEmpty(){
        for(int i=0;i<theLists.length;i++){
            theLists[i].clear();
        }
        currentSize = 0;

    }
    private void rehash(){

    }
    private int myhash(E x){
        int hashVal = x.hashCode();
        hashVal %= theLists.length;
        if(hashVal < 0){
            hashVal += theLists.length;
        }
        return hashVal;
    }
    private static int nextPrime(int n){return 1;}
    private static boolean isPrime(int n){return false;}
}
