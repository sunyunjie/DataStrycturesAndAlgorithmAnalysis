package chapter5;

public class Employee {
    private String name;
    private double salary;
    private int seniority;
    public boolean equals(Object rhs){
        return rhs instanceof Employee && name.equals(((Employee)rhs).name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
